const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');
const dotenv = require("dotenv");


const app = express();



dotenv.config();

const db = require('./config/db').database;

//database connection
mongoose.connect(db, { 
    useNewUrlParser: true,
    useUnifiedTopology: true
    }).then( () => {
            console.log('database connected successfully')
        })
        .catch((err) => {
            console.log('unable to connect with the database', err)
        });


//Define the Port
const port = process.env.PORT || 3000;

//initialize cors Midleware
app.use(cors());

//initialize bodyParser Midleware
app.use(bodyParser.json());


// app.get('*', (req, res)=>{
//     res.sendFile(path.join(__dirname, 'public/index.html'));
// });

const postRoutes = require('./routes/apis/post');
const post = require('./models/Post');

app.use('/api/posts', postRoutes);

app.get('/', function(req, res){
    res.send('<h1>Hello World</h1>')
});

app.listen(port, function(){
    console.log('server started on Port ', port)
});
