const mongoose = require('mongoose');
const PostSchema = mongoose.Schema({
    title: {
        type: String,
        require: true
    },
    body: {
        type: String,
        require: true
    },
    create_at: {
        type: Date,
        default: Date.now()
    }
});

const Post = module.exports = mongoose.model('Post', PostSchema);