const express = require('express');
const router = express.Router();

const Post = require('../../models/Post');

//get all the post
router.get('/', (req, res, next)=>{
    Post.find()
    .then((posts)=>{
        res.json({posts});
    })
    .catch(error => console.log(error))
});

//create a post
router.post('/add', (req, res, next)=>{
    const title = req.body.title;
    const body = req.body.body;
   newPost = new Post({
        title: title, 
        body: body
    });
    newPost.save()
    .then(post =>{
        res.json({post});
    })
    .catch(err=> console.log(err));
})

//To update a post
router.put('/update/:id', (req, res, next)=>{
    //Grab the id of the post
    let id = req.params.id;
    //console.log(id);
    //find the post by id from the database
    Post.findById(id)
        .then(post =>{  
            post.title = req.body.title;
            post.body = req.body.body;
            
            post.save()
                .then(post =>{
                    res.send({
                    message: 'Post updated successful',
                        status: 'success',
                        post: post
                })
            })
            .catch(err => console.log(err))
    })
    .catch(err => console.log(err));

})

//make delete request
router.delete('/:id', (req, res, next)=>{
    //Grab the id of the post
    let id = req.params.id;
  //  console.log(id);
    //find the post by id from the database
    Post.findById(id)
        .then(post =>{  
            post.delete()
                .then(post =>{
                    res.send({
                    message: 'Post deleted successful',
                        status: 'success',
                        post: post
                })
            })
            .catch(err => console.log(err))
    })
    .catch(err => console.log(err));

})

module.exports = router;